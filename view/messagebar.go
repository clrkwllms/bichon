// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/tview"
)

type MessageBar struct {
	Text *tview.TextView
}

func NewMessageBar() *MessageBar {
	return &MessageBar{
		tview.NewTextView().SetDynamicColors(true),
	}
}

func (msgbar *MessageBar) Info(msg string) {
	msgbar.Text.SetText("[darkcyan::b]" + tview.Escape(msg))
}
