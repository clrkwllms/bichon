// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"strings"
	"time"

	"github.com/danwakefield/fnmatch"
	log "github.com/sirupsen/logrus"
)

type MergeReqFilter func(mreq MergeReq) bool

func matchString(value, query string, glob, casefold bool) bool {
	if glob {
		flags := 0
		if casefold {
			flags |= fnmatch.FNM_CASEFOLD
		}
		return fnmatch.Match(query, value, flags)
	} else {
		if casefold {
			return strings.ToLower(query) == strings.ToLower(value)
		} else {
			return query == value
		}
	}
}

func MergeReqFilterReject(this MergeReqFilter) MergeReqFilter {
	log.Infof("Filter reject %p", this)
	return func(mreq MergeReq) bool {
		return !this(mreq)
	}
}

func MergeReqFilterBoth(this, that MergeReqFilter) MergeReqFilter {
	log.Infof("Filter both %p %p", this, that)
	return func(mreq MergeReq) bool {
		return this(mreq) && that(mreq)
	}
}

func MergeReqFilterEither(this, that MergeReqFilter) MergeReqFilter {
	log.Infof("Filter either %p %p", this, that)
	return func(mreq MergeReq) bool {
		return this(mreq) || that(mreq)
	}
}

func MergeReqFilterProject(project string, glob, casefold bool) MergeReqFilter {
	log.Infof("Filter project %s glob=%t casefold=%t", project, glob, casefold)
	return func(mreq MergeReq) bool {
		return matchString(mreq.Repo.Project, project, glob, casefold)
	}
}

func MergeReqFilterState(state MergeReqState) MergeReqFilter {
	log.Infof("Filter state %s", state)
	return func(mreq MergeReq) bool {
		return mreq.State == state
	}
}

func MergeReqFilterMetadataStatus(status MergeReqStatus) MergeReqFilter {
	log.Infof("Filter status %s", status)
	return func(mreq MergeReq) bool {
		return mreq.Metadata.Status == status
	}
}

func MergeReqFilterAge(dur time.Duration) MergeReqFilter {
	log.Infof("Filter age %s", dur)
	then := time.Now().Add(dur * -1)
	return func(mreq MergeReq) bool {
		return mreq.CreatedAt.After(then)
	}
}

func MergeReqFilterActivity(dur time.Duration) MergeReqFilter {
	log.Infof("Filter activity %s", dur)
	then := time.Now().Add(dur * -1)
	return func(mreq MergeReq) bool {
		return mreq.UpdatedAt.After(then)
	}
}

func MergeReqFilterSubmitterRealName(name string, glob, casefold bool) MergeReqFilter {
	log.Infof("Filter real name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq MergeReq) bool {
		return matchString(mreq.Submitter.RealName, name, glob, casefold)
	}
}

func MergeReqFilterSubmitterUserName(name string, glob, casefold bool) MergeReqFilter {
	log.Infof("Filter user name %s glob=%t casefold=%t", name, glob, casefold)
	return func(mreq MergeReq) bool {
		return matchString(mreq.Submitter.UserName, name, glob, casefold)
	}
}

func MergeReqFilterLabels(label string) MergeReqFilter {
	log.Infof("Filter label %s", label)
	return func(mreq MergeReq) bool {
		for _, mrlabel := range mreq.Labels {
			if mrlabel == label {
				return true
			}
		}
		return false
	}
}
