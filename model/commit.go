// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"time"

	log "github.com/sirupsen/logrus"

	udiff "gitlab.com/bichon-project/bichon/diff"
)

type Commit struct {
	Hash      string    `json:"hash"`
	Title     string    `json:"title"`
	Author    User      `json:"author"`
	Committer User      `json:"committer"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Message   string    `json:"message"`
	Diffs     []Diff    `json:"diffs"`

	Metadata CommitMetadata `json:"bichonMetadata"`
}

type CommitMetadata struct {
	Partial bool `json:"partial"`
}

type Diff struct {
	Content     string `json:"content"`
	NewFile     string `json:"newFile"`
	OldFile     string `json:"oldFile"`
	NewMode     string `json:"newMode"`
	OldMode     string `json:"oldMode"`
	CreatedFile bool   `json:"createdFile"`
	RenamedFile bool   `json:"renamedFile"`
	DeletedFile bool   `json:"deletedFile"`
}

func (commit *Commit) Age() string {
	age := time.Since(commit.CreatedAt)
	return formatDuration(age)
}

func (commit *Commit) Activity() string {
	age := time.Since(commit.UpdatedAt)
	return formatDuration(age)
}

func (commit *Commit) GetFile(filename string) *Diff {
	for idx, _ := range commit.Diffs {
		diff := &commit.Diffs[idx]
		if diff.NewFile == filename {
			return diff
		}
	}

	return nil
}

// If @line is part of the diff context new lines, then return
// the old line number in the context. If @line is either added
// or removed, then return 0.
func (diff *Diff) Revert(line uint) (uint, error) {
	hunks, err := udiff.ParseUnifiedDiffHunks(diff.Content)
	if err != nil {
		return 0, err
	}

	newLine := line
	for idx, hunk := range hunks {
		log.Infof("Hunk old %d:%d new %d:%d", hunk.OldLine, hunk.OldCount, hunk.NewLine, hunk.NewCount)
		if line >= hunk.NewLine {
			distance := line - hunk.NewLine
			if distance < hunk.NewCount {
				log.Infof("Line %d inside hunk %d distance %d", line, idx, distance)
				oldLine := hunk.OldLine
				for _, line := range hunk.Lines {
					if distance == 0 {
						if line.Type == udiff.DIFF_LINE_CONTEXT {
							return oldLine, nil
						} else {
							return 0, nil
						}
					}
					if line.Type == udiff.DIFF_LINE_CONTEXT || line.Type == udiff.DIFF_LINE_ADDED {
						distance--
					}
					if line.Type == udiff.DIFF_LINE_CONTEXT || line.Type == udiff.DIFF_LINE_REMOVED {
						oldLine++
					}
				}

				// This should be unreachable
				log.Errorf("Reached the unreachable")
				return 0, nil
			} else {
				delta := distance - hunk.NewCount
				log.Infof("Line %d after hunk %d delta %d", line, idx, delta)
				newLine = hunk.OldLine + hunk.OldCount + delta
			}
		} else {
			log.Infof("Line %d before hunk %d, stopping", line, idx)
			// before this hunk, we've done far enough now
			break
		}
	}

	log.Infof("Rebased line %d to %d", line, newLine)
	return newLine, nil
}
