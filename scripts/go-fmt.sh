#!/bin/sh

GOFMT=$(go env GOROOT)/bin/gofmt

git ls-tree -r --name-only HEAD |  grep '\.go' | xargs $GOFMT -d -e > go-fmt.patch

if test -s go-fmt.patch
then
    echo "Files failed go fmt code style check"
    cat go-fmt.patch
    exit 1
fi
