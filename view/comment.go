// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

func colorfyComment(data, indent string) (lines []string) {
	data = tview.Escape(data)
	colorQuoted :=
		GetStyleMarker(
			ELEMENT_COMMENT_QUOTED_TEXT,
			ELEMENT_COMMENT_QUOTED_FILL,
			ELEMENT_COMMENT_QUOTED_ATTR)
	colorReset := "[-:-:-]"

	lines = strings.Split(data, "\n")
	for idx, _ := range lines {
		if len(lines[idx]) > 0 && lines[idx][0] == '>' {
			lines[idx] = colorQuoted + indent + lines[idx] + colorReset
		} else {
			lines[idx] = indent + lines[idx]
		}
	}

	return lines
}

func FormatThreads(threads []model.CommentThread, filemarkers bool, region *int, indent string) (lines []string, regions []PatchRegion) {
	colorHeader :=
		GetStyleMarker(
			ELEMENT_SUMMARY_HEADER_TEXT,
			ELEMENT_SUMMARY_HEADER_FILL,
			ELEMENT_SUMMARY_HEADER_ATTR)
	colorDate :=
		GetStyleMarker(
			ELEMENT_SUMMARY_DATE_TEXT,
			ELEMENT_SUMMARY_DATE_FILL,
			ELEMENT_SUMMARY_DATE_ATTR)
	colorAuthor :=
		GetStyleMarker(
			ELEMENT_SUMMARY_AUTHOR_TEXT,
			ELEMENT_SUMMARY_AUTHOR_FILL,
			ELEMENT_SUMMARY_AUTHOR_ATTR)
	for tidx, thread := range threads {
		var tlines []string
		for cidx, comment := range thread.Comments {
			indent := ""
			var header string
			if cidx == 0 {
				header = colorHeader + fmt.Sprintf("Comment #%d by ", tidx+1) + "[-:-:-]"
			} else {
				indent = "   "
				header = colorHeader + indent + "Reply by [-:-:-]"
			}
			header += colorAuthor + tview.Escape(comment.Author.Name) + "[-:-:-]" +
				colorHeader + " on " + "[-:-:-]" +
				colorDate + comment.CreatedAt.Format(time.RFC1123) + "[-:-:-]"
			tlines = append(tlines, header)

			tlines = append(tlines, indent)

			if comment.Context != nil && cidx == 0 && filemarkers {
				ctx := comment.Context
				if ctx.NewFile != "" {
					tlines = append(tlines,
						fmt.Sprintf("File %s:%d (%s)", tview.Escape(ctx.NewFile), ctx.NewLine, ctx.HeadHash))
				} else {
					tlines = append(tlines,
						fmt.Sprintf("File %s:%d (%s)", tview.Escape(ctx.OldFile), ctx.OldLine, ctx.HeadHash))
				}
				tlines = append(tlines, indent)
			}
			desc := colorfyComment(comment.Description, indent)
			tlines = append(tlines, desc...)
			tlines = append(tlines, indent, indent)
		}

		info := PatchRegion{
			IDStart: *region,
			IDEnd:   *region + (len(tlines) - 1),
			Thread:  thread.ID,
		}

		for idx, _ := range tlines {
			*region++
			tlines[idx] = fmt.Sprintf("[\"l%d\"]%s[\"\"]%s", *region, indent, tlines[idx])
		}

		regions = append(regions, info)
		lines = append(lines, tlines...)
	}

	return
}
