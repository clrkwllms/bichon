// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

type Series struct {
	Index     int      `json:"index"`
	Version   int      `json:"version"`
	BaseHash  string   `json:"baseHash"`
	StartHash string   `json:"startHash"`
	HeadHash  string   `json:"headHash"`
	Patches   []Commit `json:"patches"`

	Metadata SeriesMetadata `json:"bichonMetadata"`
}

type SeriesMetadata struct {
	Partial bool `json:"partial"`
}
