module gitlab.com/bichon-project/bichon

go 1.12

require (
	github.com/alecthomas/participle v0.4.4
	github.com/casimir/xdg-go v0.0.0-20160329195404-372ccc2180da
	github.com/danieljoos/wincred v1.1.0 // indirect
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
	github.com/gdamore/tcell v1.3.0
	github.com/go-ini/ini v1.56.0
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.6.6 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/xanzy/go-gitlab v0.32.0
	github.com/zalando/go-keyring v0.0.0-20200121091418-667557018717
	gitlab.com/bichon-project/tview v0.0.0-20200527125235-77fd4ee44359
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.24.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
