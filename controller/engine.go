// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package controller

import (
	"crypto/tls"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/cache"
	"gitlab.com/bichon-project/bichon/config/xdg"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
)

type Engine interface {
	AddRepository(repo model.Repo)
	UpdateRepository(repo model.Repo)
	RemoveRepository(repo model.Repo)

	RefreshRepos()
	RefreshMergeRequest(mreq model.MergeReq)

	LoadMergeRequestSeriesPatches(mreq model.MergeReq, series model.Series)
	LoadMergeRequestCommitDiffs(mreq model.MergeReq, commit model.Commit)

	AddMergeRequestComment(mreq model.MergeReq, text string)
	AddMergeRequestThread(mreq model.MergeReq, text string, context *model.CommentContext)
	AddMergeRequestReply(mreq model.MergeReq, thread, text string)

	MarkRead(mreq model.MergeReq)

	AcceptMergeRequest(mreq model.MergeReq)
	ApproveMergeRequest(mreq model.MergeReq)
	UnapproveMergeRequest(mreq model.MergeReq)
}

type engineJob interface {
	Run(engine *engineImpl)
}

type engineImplRepo struct {
	Repo model.Repo
	// Once added the MergeReq should be treated as immutable
	// To change it, create a copy, change the copy and assign
	// that copy back into this map
	MergeRequests map[uint]*model.MergeReq
	Removed       bool
	Source        source.Source
}

type engineAddRepoJob struct {
	Repo model.Repo
}

type engineUpdateRepoJob struct {
	Repo model.Repo
}

type engineRemoveRepoJob struct {
	Repo model.Repo
}

type engineRefreshReposJob struct {
}

type engineRefreshMergeRequestJob struct {
	MergeReq model.MergeReq
}

type engineLoadMergeRequestSeriesPatchesJob struct {
	MergeReq model.MergeReq
	Series   model.Series
}

type engineLoadMergeRequestCommitDiffsJob struct {
	MergeReq model.MergeReq
	Commit   model.Commit
}

type engineAddMergeRequestCommentJob struct {
	MergeReq model.MergeReq
	Text     string
}

type engineAddMergeRequestThreadJob struct {
	MergeReq model.MergeReq
	Text     string
	Context  *model.CommentContext
}

type engineAddMergeRequestReplyJob struct {
	MergeReq model.MergeReq
	Thread   string
	Text     string
}

type engineMergeRequestMarkReadJob struct {
	MergeReq model.MergeReq
}

type engineMergeRequestAcceptJob struct {
	MergeReq model.MergeReq
}

type engineMergeRequestApproveJob struct {
	MergeReq model.MergeReq
}

type engineMergeRequestUnapproveJob struct {
	MergeReq model.MergeReq
}

type engineImpl struct {
	// Immutable
	RefreshInterval time.Duration
	TLSConfig       *tls.Config
	Listener        Listener

	// Only access from updateLoop goroutine
	Cache cache.Cache
	Repos []engineImplRepo

	// For comms with updateLoop goroutine
	RefreshJob *time.Timer
	Jobs       chan engineJob
}

func NewEngine(listener Listener, tlscfg *tls.Config) Engine {
	mreqcache := cache.NewFileCache(xdg.CachePath("merge-requests"))

	engine := &engineImpl{
		RefreshInterval: time.Minute * 15,
		TLSConfig:       tlscfg,

		Listener: listener,

		Cache: mreqcache,

		RefreshJob: time.NewTimer(0), // Immediate expire first time
		Jobs:       make(chan engineJob, 1000),
	}

	go engine.updateLoop()

	return engine
}

func (engine *engineImpl) loadMergeRequestCache(enginerepo *engineImplRepo) {
	engine.Listener.Status(fmt.Sprintf("Loading merge request cache"))
	mreqids, _ := engine.Cache.ListMergeRequests(&enginerepo.Repo)
	engine.Listener.Status(fmt.Sprintf("Loading %d merge requests from cache", len(mreqids)))

	for idx, id := range mreqids {
		engine.Listener.Status(fmt.Sprintf("Loading merge request %d from cache [%d/%d]",
			id, idx+1, len(mreqids)))
		mreq, err := engine.Cache.LoadMergeRequest(&enginerepo.Repo, id)
		if err != nil {
			continue
		}

		enginerepo.MergeRequests[mreq.ID] = mreq
		engine.Listener.MergeRequestNotify(mreq)
	}
	engine.Listener.Status("")
}

func (engine *engineImpl) refreshMergeRequest(enginerepo *engineImplRepo, mreq *model.MergeReq) {
	versions, err := enginerepo.Source.GetVersions(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request versions: %s", err))
		return
	}

	for idx, _ := range versions {
		ver := &versions[idx]
		for _, oldver := range mreq.Versions {
			if ver.Version == oldver.Version {
				log.Infof("Copying patches %d for version %d", len(oldver.Patches), oldver.Version)
				ver.Patches = oldver.Patches
				ver.Metadata.Partial = false
			}
		}

		// We are only loading the most recent version by default
		// We'll add support for lazy loading older versions
		// once we add UI for choosing to display other versions
		if idx == (len(versions)-1) && ver.Metadata.Partial == true {
			patches, err := enginerepo.Source.GetPatches(mreq, ver)
			if err != nil {
				engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request patches: %s", err))
				continue
			}
			log.Infof("Fetching new patches %d", len(patches))

			ver.Patches = patches
			ver.Metadata.Partial = false
		}
	}

	mreq.Versions = versions

	threads, err := enginerepo.Source.GetMergeRequestThreads(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request threads: %s", err))
		return
	}

	mreq.Threads = threads

	mreq.Metadata.Partial = false

	err = engine.Cache.SaveMergeRequest(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
			mreq.ID, err))
	}
}

func (engine *engineImpl) refreshMergeRequests(enginerepo *engineImplRepo) {
	log.Infof("Refresh merged requests %s", enginerepo.Repo.String())
	engine.Cache.LoadRepo(&enginerepo.Repo)
	// By default we want to query merge requests in all
	// states, so that we see transitions.
	//
	// The first time around, however, we're only going
	// to ask for open mreqs, as we don't need anything
	// that was closed before we got involved
	onlyOpen := false
	if enginerepo.Repo.UpdatedAt == nil {
		log.Infof("No updatedAt time for repo %s", enginerepo.Repo.String())
		var newest *time.Time
		for _, mreq := range enginerepo.MergeRequests {
			if newest == nil {
				newest = &mreq.UpdatedAt
			} else {
				if mreq.UpdatedAt.After(*newest) {
					newest = &mreq.UpdatedAt
				}
			}
		}

		if newest == nil {
			onlyOpen = true
		} else {
			enginerepo.Repo.UpdatedAt = newest
			log.Infof("Newest mreq %s for repo %s", newest.String(), enginerepo.Repo.String())
		}
	}

	engine.Listener.Status(fmt.Sprintf("Querying current merge requests %s", enginerepo.Repo.String()))
	then := time.Now().UTC()
	mreqs, err := enginerepo.Source.GetMergeRequests(onlyOpen, enginerepo.Repo.UpdatedAt)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to query merge requests: %s", err))
		return
	}

	enginerepo.Repo.UpdatedAt = &then

	mreqsToUpdate := make([]model.MergeReq, 0)
	for _, mreq := range mreqs {
		cachedmreq, ok := enginerepo.MergeRequests[mreq.ID]

		if ok {
			// We never want to loose info, so we'll copy the cached
			// versions and refresh it later
			mreq.Versions = cachedmreq.Versions
			mreq.Metadata.Status = model.STATUS_UPDATED

			if cachedmreq.UpdatedAt.Before(mreq.UpdatedAt) || cachedmreq.Metadata.Partial {
				log.Infof("Mreq %s is outdated", mreq.String())
				mreqsToUpdate = append(mreqsToUpdate, mreq)
			} else {
				log.Infof("Mreq %s is up2date", mreq.String())
			}
		} else {
			log.Infof("Mreq %s is not cached", mreq.String())
			mreqsToUpdate = append(mreqsToUpdate, mreq)
		}
	}
	mreqs = mreqsToUpdate

	if len(mreqs) > 0 {
		engine.Listener.Status(fmt.Sprintf("Fetching %d merge requests", len(mreqs)))
	}
	for idx, _ := range mreqs {
		mreq := &mreqs[idx]
		engine.Listener.Status(fmt.Sprintf("Fetching merge request %d from source [%d/%d]",
			mreq.ID, idx+1, len(mreqs)))
		engine.refreshMergeRequest(enginerepo, mreq)

		enginerepo.MergeRequests[mreq.ID] = mreq
		engine.Listener.MergeRequestNotify(mreq)
	}
	engine.Listener.Status("")
	engine.Cache.SaveRepo(&enginerepo.Repo)
}

func (engine *engineImpl) refreshJob() {
	log.Info("Periodic refresh job running")
	for idx, _ := range engine.Repos {
		repo := &engine.Repos[idx]
		log.Infof("Refreshing repo %s", repo.Repo.String())
		engine.refreshMergeRequests(repo)
	}
}

func (engine *engineImpl) addRepo(repo model.Repo) (*engineImplRepo, error) {
	log.Infof("Adding repo %s", repo.String())

	sourceimpl, err := source.NewGitLabForRepo(engine.TLSConfig, &repo)
	if err != nil {
		return nil, err
	}

	err = engine.Cache.AddRepo(&repo)
	if err != nil {
		return nil, err
	}

	repoimpl := engineImplRepo{
		Repo:          repo,
		MergeRequests: make(map[uint]*model.MergeReq),
		Source:        sourceimpl,
	}

	engine.Repos = append(engine.Repos, repoimpl)

	return &engine.Repos[len(engine.Repos)-1], nil
}

func (job *engineAddRepoJob) Run(engine *engineImpl) {
	log.Infof("Add repo job %s", job.Repo.String())
	repoimpl, err := engine.addRepo(job.Repo)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to add repo %s: %s",
			job.Repo.String(), err))
		return
	}

	engine.loadMergeRequestCache(repoimpl)
	engine.refreshMergeRequests(repoimpl)

	engine.Listener.RepoAdded(job.Repo)
}

func (engine *engineImpl) updateRepo(repo model.Repo) (*engineImplRepo, error) {
	log.Infof("Update repo %s", repo.String())

	sourceimpl, err := source.NewGitLabForRepo(engine.TLSConfig, &repo)
	if err != nil {
		return nil, err
	}

	for idx, _ := range engine.Repos {
		repoimpl := &engine.Repos[idx]

		if repo.Server == repoimpl.Repo.Server &&
			repo.Project == repoimpl.Repo.Project {
			repoimpl.Repo = repo
			repoimpl.Source = sourceimpl
			return repoimpl, nil
		}
	}

	return nil, fmt.Errorf("No current repo matching %s", repo.String())
}

func (job *engineUpdateRepoJob) Run(engine *engineImpl) {
	log.Infof("Update repo job %s", job.Repo.String())
	repoimpl, _ := engine.updateRepo(job.Repo)
	if repoimpl != nil {
		engine.refreshMergeRequests(repoimpl)
	}
}

func (job *engineRemoveRepoJob) Run(engine *engineImpl) {
	log.Infof("Remove repo job %s", job.Repo.String())

	for idx, impl := range engine.Repos {
		if impl.Repo.Equal(&job.Repo) {
			engine.Repos = append(engine.Repos[0:idx], engine.Repos[idx+1:]...)
			break
		}
	}

	engine.Listener.RepoRemoved(job.Repo)
}

func (job *engineRefreshReposJob) Run(engine *engineImpl) {
	log.Info("Refresh repos job")

	engine.RefreshJob.Stop()
	engine.refreshJob()
	engine.RefreshJob.Reset(engine.RefreshInterval)
}

func (engine *engineImpl) getEngineRepo(repo model.Repo) *engineImplRepo {
	for idx, _ := range engine.Repos {
		impl := &engine.Repos[idx]
		if impl.Repo.Equal(&repo) {
			return impl
		}
	}

	return nil
}

func (job *engineLoadMergeRequestSeriesPatchesJob) Run(engine *engineImpl) {
	log.Info("Load series patches job")

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Loading %s patches %d %d",
		job.MergeReq.String(), job.Series.Index, job.Series.Version))
	patches, err := impl.Source.GetPatches(&job.MergeReq, &job.Series)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to load commit diffs: %s", err))
	} else {
		newmreq := *impl.MergeRequests[job.MergeReq.ID]
		for sidx, _ := range newmreq.Versions {
			series := &newmreq.Versions[sidx]
			if series.Index == job.Series.Index {
				series.Patches = patches
			}
		}

		impl.MergeRequests[newmreq.ID] = &newmreq
		err := engine.Cache.SaveMergeRequest(&newmreq)
		if err != nil {
			engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
				newmreq.ID, err))
		}
		log.Infof("Loaded series %s patches %d %d", newmreq.String(), job.Series.Index, job.Series.Version)
		engine.Listener.MergeRequestNotify(&newmreq)
		engine.Listener.Status("")
	}
}

func (job *engineLoadMergeRequestCommitDiffsJob) Run(engine *engineImpl) {
	log.Info("Load commit diffs job")

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Loading %s diff %s",
		job.MergeReq.String(), job.Commit.Hash))
	diffs, err := impl.Source.GetCommitDiffs(&job.MergeReq, &job.Commit)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to load commit diffs: %s", err))
	} else {
		newmreq := *impl.MergeRequests[job.MergeReq.ID]
		for sidx, _ := range newmreq.Versions {
			series := &newmreq.Versions[sidx]
			for cidx, _ := range series.Patches {
				commit := &series.Patches[cidx]
				if commit.Hash == job.Commit.Hash {
					commit.Diffs = diffs
					commit.Metadata.Partial = false
				}
			}
		}

		impl.MergeRequests[newmreq.ID] = &newmreq
		err := engine.Cache.SaveMergeRequest(&newmreq)
		if err != nil {
			engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
				newmreq.ID, err))
		}
		log.Infof("Loaded diff %s diff %s", newmreq.String(), job.Commit.Hash)
		engine.Listener.MergeRequestNotify(&newmreq)
		engine.Listener.Status("")
	}
}

func (job *engineRefreshMergeRequestJob) Run(engine *engineImpl) {
	log.Infof("Refresh merge req job %s", job.MergeReq.String())
	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Fetching merge request %d from source",
		job.MergeReq.ID))
	newmreq, err := impl.Source.GetMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to refresh merge request: %s", err))
		return
	}
	newmreq.Versions = job.MergeReq.Versions
	newmreq.Threads = job.MergeReq.Threads
	engine.refreshMergeRequest(impl, &newmreq)
	newmreq.Metadata.Status = model.STATUS_UPDATED
	impl.MergeRequests[job.MergeReq.ID] = &newmreq
	engine.Listener.Status("")
	engine.Listener.MergeRequestNotify(&newmreq)
}

func (job *engineAddMergeRequestCommentJob) Run(engine *engineImpl) {
	log.Infof("Add merge request comment job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting comment to %s", job.MergeReq.String()))
	err := impl.Source.AddMergeRequestComment(&job.MergeReq, job.Text)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post comment: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineAddMergeRequestThreadJob) Run(engine *engineImpl) {
	log.Infof("Add merge req thread job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting thread to %s", job.MergeReq.String()))
	err := impl.Source.AddMergeRequestThread(&job.MergeReq, job.Text, job.Context)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post thread: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineAddMergeRequestReplyJob) Run(engine *engineImpl) {
	log.Infof("Add merge req reply job %s %s", job.MergeReq.String(), job.Thread)

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting reply to %s thread %s",
		job.MergeReq.String(), job.Thread))
	err := impl.Source.AddMergeRequestReply(&job.MergeReq, job.Thread, job.Text)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post reply: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineMergeRequestMarkReadJob) Run(engine *engineImpl) {
	log.Infof("Mark read job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	newmreq := *impl.MergeRequests[job.MergeReq.ID]
	newmreq.Metadata.Status = model.STATUS_READ
	impl.MergeRequests[job.MergeReq.ID] = &newmreq
	err := engine.Cache.SaveMergeRequest(&newmreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
			newmreq.ID, err))
	}
	log.Infof("Mark read %s", newmreq.String())
	engine.Listener.MergeRequestNotify(&newmreq)
}

func (job *engineMergeRequestAcceptJob) Run(engine *engineImpl) {
	log.Infof("Merge request accept job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.AcceptMergeRequest(&job.MergeReq, true)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot accept merge request: %s", err))
	} else {
		engine.Listener.Status(fmt.Sprintf("Accepted merge request %s", job.MergeReq.String()))
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineMergeRequestApproveJob) Run(engine *engineImpl) {
	log.Infof("Merge request approve job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.ApproveMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot approve merge request: %s", err))
	} else {
		engine.Listener.Status(fmt.Sprintf("Approved merge request %s", job.MergeReq.String()))
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineMergeRequestUnapproveJob) Run(engine *engineImpl) {
	log.Infof("Merge request unapprove job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.UnapproveMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot unapprove merge request: %s", err))
	} else {
		engine.Listener.Status(fmt.Sprintf("Unapproved merge request %s", job.MergeReq.String()))
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (engine *engineImpl) updateLoop() {
	log.Infof("Update loop running")
	for idx, _ := range engine.Repos {
		repo := &engine.Repos[idx]
		log.Infof("Loading cache for repo %s", repo.Repo.String())
		engine.loadMergeRequestCache(repo)
	}

	for {
		log.Info("Selecting job")
		select {
		case <-engine.RefreshJob.C:
			log.Info("Refresh job")
			engine.refreshJob()
			engine.RefreshJob.Reset(engine.RefreshInterval)

		case job := <-engine.Jobs:
			job.Run(engine)
		}

		log.Info("Next loop iteration")
	}
}

func (engine *engineImpl) AddRepository(repo model.Repo) {
	log.Infof("Queue add repo %s", repo.String())
	engine.Jobs <- &engineAddRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) UpdateRepository(repo model.Repo) {
	log.Infof("Queue update repo %s", repo.String())
	engine.Jobs <- &engineUpdateRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) RemoveRepository(repo model.Repo) {
	log.Infof("Queue remove repo %s", repo.String())
	engine.Jobs <- &engineRemoveRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) RefreshRepos() {
	log.Info("Queue refresh repos")
	engine.Jobs <- &engineRefreshReposJob{}
}

func (engine *engineImpl) RefreshMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue refresh merge request %s", mreq.String())
	engine.Jobs <- &engineRefreshMergeRequestJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) LoadMergeRequestCommitDiffs(mreq model.MergeReq, commit model.Commit) {
	log.Infof("Queue load merge request commit diffs %s diff %s",
		mreq.String(), commit.Hash)
	engine.Jobs <- &engineLoadMergeRequestCommitDiffsJob{
		MergeReq: mreq,
		Commit:   commit,
	}
}

func (engine *engineImpl) LoadMergeRequestSeriesPatches(mreq model.MergeReq, series model.Series) {
	log.Infof("Queue load merge request series patches %s series %d %d",
		mreq.String(), series.Index, series.Version)
	engine.Jobs <- &engineLoadMergeRequestSeriesPatchesJob{
		MergeReq: mreq,
		Series:   series,
	}
}

func (engine *engineImpl) AddMergeRequestComment(mreq model.MergeReq, text string) {
	log.Infof("Queue add merge request comment job %s", mreq.String())
	engine.Jobs <- &engineAddMergeRequestCommentJob{
		MergeReq: mreq,
		Text:     text,
	}
}

func (engine *engineImpl) AddMergeRequestThread(mreq model.MergeReq, text string, context *model.CommentContext) {
	log.Infof("Queue add merge request thread %s", mreq.String())
	engine.Jobs <- &engineAddMergeRequestThreadJob{
		MergeReq: mreq,
		Text:     text,
		Context:  context,
	}
}

func (engine *engineImpl) AddMergeRequestReply(mreq model.MergeReq, thread, text string) {
	log.Infof("Queue add merge request reply %s", mreq.String())
	engine.Jobs <- &engineAddMergeRequestReplyJob{
		MergeReq: mreq,
		Thread:   thread,
		Text:     text,
	}
}

func (engine *engineImpl) MarkRead(mreq model.MergeReq) {
	log.Infof("Queue mark read job %s", mreq.String())
	engine.Jobs <- &engineMergeRequestMarkReadJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) AcceptMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue merge request accept %s", mreq.String())
	engine.Jobs <- &engineMergeRequestAcceptJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) ApproveMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue merge request approve %s", mreq.String())
	engine.Jobs <- &engineMergeRequestApproveJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) UnapproveMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue merge request unapprove %s", mreq.String())
	engine.Jobs <- &engineMergeRequestUnapproveJob{
		MergeReq: mreq,
	}
}
