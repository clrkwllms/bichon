// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	log "github.com/sirupsen/logrus"
)

type MergeReqList struct {
	Master []*MergeReq
	Active []*MergeReq
	Filter MergeReqFilter
	Sorter MergeReqSorter
}

func (mreqs *MergeReqList) Insert(mreq *MergeReq) {
	found := false
	for idx, oldmreq := range mreqs.Master {
		if oldmreq.Equal(mreq) {
			mreqs.Master[idx] = mreq
			found = true
			log.Info("Updated existing mreq")
			break
		}
	}
	if !found {
		log.Info("Got new mreq")
		mreqs.Master = append(mreqs.Master, mreq)
	}

	mreqs.ReFilter()
}

func (mreqs *MergeReqList) PurgeRepo(repo *Repo) {
	newmreqs := []*MergeReq{}
	for _, mreq := range mreqs.Master {
		if !mreq.Repo.Equal(repo) {
			newmreqs = append(newmreqs, mreq)
		}
	}
	mreqs.Master = newmreqs
	mreqs.ReFilter()
}

func (mreqs *MergeReqList) ReSort() {
	mreqs.Active = MergeReqSort(mreqs.Active, mreqs.Sorter)
}

func (mreqs *MergeReqList) ReFilter() {
	if mreqs.Filter == nil {
		mreqs.Active = MergeReqSort(mreqs.Master, mreqs.Sorter)
	} else {
		active := make([]*MergeReq, 0)

		for _, mreq := range mreqs.Master {
			if mreqs.Filter(*mreq) {
				active = append(active, mreq)
			}
		}

		mreqs.Active = MergeReqSort(active, mreqs.Sorter)
	}
}
