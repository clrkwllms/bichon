// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/model/filterstring"
	"gitlab.com/bichon-project/tview"
)

const helpMessage = ` Expression: Term ( ('&' | '|') Term)*
 Term: '!'? Match | '(' Expression ')'
 Match: Project | Real name | User name | Age |
        Activity | State | Status | Label
 Project name: ~p STRING  (glob match)
 Real name:    ~n STRING  (glob match)
 User name:    ~u STRING  (glob match)
 Age:          ~a TIME
 Activity:     ~t TIME
 State:        ~s o|c|m|l|open|closed|merged|locked
 Status:       ~m n|u|o|r|new|updated|old|read
 Label:        ~l STRING  (exact match)

 STRING = a-Z, 0-9, -, /, *, ?, [, ], ., : | "..." | '...'
 TIME = 0-9 year|month|mon|week|day|hour|minute|min|second|sec

 e.g.  ~p /bichon & ~su berrange & ~t 1 week & ! ~m read
`

type QuickFilterForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener FilterFormListener
	Error    *tview.TextView

	Filter       *tview.InputField
	FilterString string
}

func (form *QuickFilterForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.FilterFormCancel()
	form.Error.SetText("")
	form.Filter.SetText(form.FilterString)
}

func (form *QuickFilterForm) confirmFunc() {
	form.Form.SetFocus(0)
	filterString := form.Filter.GetText()
	filter, err := form.GetMergeReqFilter(filterString)
	if err == nil {
		form.Error.SetText("")
		form.FilterString = filterString
		form.Listener.FilterFormConfirm(filter)
	} else {
		form.Error.SetText("[red::]" + err.Error() + "[::]")
	}
}

func NewQuickFilterForm(listener FilterFormListener) *QuickFilterForm {

	form := &QuickFilterForm{
		Form:     tview.NewForm(),
		Error:    tview.NewTextView(),
		Listener: listener,
	}

	form.Error.SetDynamicColors(true)

	layout := tview.NewFlex()
	help := tview.NewTextView()
	help.SetText(helpMessage)
	help.SetBackgroundColor(tview.Styles.ContrastBackgroundColor)
	form.Error.SetBackgroundColor(tview.Styles.ContrastBackgroundColor)
	layout.SetDirection(tview.FlexRow)
	layout.AddItem(form.Form, 7, 0, true)
	layout.AddItem(form.Error, 3, 0, false)
	layout.AddItem(help, 0, 1, false)
	form.Primitive = Modal(layout, 68, 27)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.SetBorder(true).
		SetTitle("Merge request filtering")

	form.Filter = addInputField(form.Form, "Filter", "", 60, false)
	form.Filter.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyEnter {
			form.confirmFunc()
			return nil
		}
		return event
	})
	form.Form.AddButton("Apply", form.confirmFunc)
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *QuickFilterForm) GetMergeReqFilter(filterString string) (model.MergeReqFilter, error) {
	expr, err := filterstring.NewExpression(filterString)
	if err != nil {
		log.Infof("Failed to parse expression '%s': %s", filterString, err)
		return nil, err
	}
	return expr.BuildFilter(), nil
}
